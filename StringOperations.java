import java.util.*;

public class StringOperations {
	//Print duplicate characters from string - Take sample string and print all duplicate characters.
    public static void printDuplicateCharacters(String str){
        if(null != str && !str.isEmpty()) {
            HashMap<Character, Integer> hm = new HashMap<Character, Integer>();
            str = str.replaceAll("\\s","");//replaces all white spaces
            for(int i=0; i< str.length() ; i++)
            {
                Character ch = str.charAt(i);
                if(hm.containsKey(ch))
                {
                    int value = hm.get(ch);
                    hm.put(ch, ++value);
                }
                else{
                    hm.put(ch, 1);
            }
            }
            Boolean isDuplicatePresent = false;
            ArrayList<Character> duplicateChars = new ArrayList<Character>();
            for(Map.Entry<Character,Integer> entry : hm.entrySet()){
                if(entry.getValue() >1 ){
                    isDuplicatePresent = true;
                    duplicateChars.add(entry.getKey());
                }
            }
            if(isDuplicatePresent) {
                System.out.println("The duplicate characters are: " + duplicateChars);

            }
            else
                System.out.println("No duplicates present");
        }
        else{
            System.out.println("Please enter a valid string");
        }
    }
	//Find first non repeated character of String - Take sample String and print first non-duplicate character.
    public static void printFirstNonRepeatedCharacter(String str){
        if(null != str && !str.isEmpty()) {
            HashMap<Character, Integer> hm = new LinkedHashMap<Character, Integer>();
            str = str.replaceAll("\\s","");//replaces all white spaces
            for(int i=0; i< str.length() ; i++)
            {
                Character ch = str.charAt(i);
                if(hm.containsKey(ch))
                {
                    int value = hm.get(ch);
                    hm.put(ch, ++value);
                }
                else{
                    hm.put(ch, 1);
                }
            }
            Boolean isNonRepeatedCharacterPresent = false;
            ArrayList<Character> duplicateChars = new ArrayList<Character>();
            for(Map.Entry<Character,Integer> entry : hm.entrySet()){
                if(entry.getValue() == 1 ){
                    System.out.println("The first non repeated character is: " + entry.getKey());
                    isNonRepeatedCharacterPresent = true;
                    return;
                }
            }
            if(!isNonRepeatedCharacterPresent) {
                System.out.println("No non repeated character is present");
            }
        }
        else{
            System.out.println("Please enter a valid string");
        }
    }
    public static void main(String[] args){
        System.out.println("Enter a String");
        Scanner sc= new Scanner(System.in);
        String str = sc.nextLine();
        System.out.println("You have entered :"+str);
        printDuplicateCharacters(str);
        printFirstNonRepeatedCharacter(str);
    }
}
