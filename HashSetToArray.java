import java.util.*;
public class HashSetToArray {

    public static void main(String ...args){
        HashSet<String> hset = new HashSet<>();
        hset.add("India");
        hset.add("Japan");
        hset.add("Germany");
        hset.add("Singapore");
        hset.add("Japan");
        System.out.println("HashSet elements : "+ hset);
        String[] arr = new String[hset.size()];
        hset.toArray(arr);
        System.out.println("Array elements: ");
        for(String str : arr){
            System.out.print(str +" ");
        }
    }
}
