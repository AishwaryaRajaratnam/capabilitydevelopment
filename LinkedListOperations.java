import java.util.*;
class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val)
        {
            this.val = val;
        }
     ListNode(int val, ListNode next)
        {
            this.val = val; this.next = next;
        }

      //ListNode createLinkedList
 }
class LinkedList {
    ListNode head;

    public static ListNode createList(){
        LinkedList ll = new LinkedList();
        Scanner sc = new Scanner(System.in);
        String inputLine;
        while(sc.hasNextLine()) {
            inputLine = sc.nextLine();
            if(inputLine.length()==0) break;
            int val = Integer.parseInt(inputLine);
            ListNode node = new ListNode(val);
            node.next = null;
            if (ll.head == null) {
                ll.head = node;
            }
            else{
                ListNode last = ll.head;
                while (last.next != null) {
                    last = last.next;
                }
                last.next = node;
            }

        }
        return ll.head;
    }
    public static void printList(ListNode head) {
        System.out.print("LinkList : ");
        ListNode currNode = head;
        while (currNode != null) {
            System.out.print(currNode.val + " ");
            currNode = currNode.next;
        }
        System.out.println();
    }
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2){
        ListNode result = new ListNode(-1);
        ListNode head = result;
        int carry = 0;

        while(l1!=null || l2!=null){
            int value1 = (l1!=null)?l1.val:0;
            int value2 = (l2!=null)?l2.val:0;
            int sum = (value1+value2+carry)%10;
            carry = (value1+value2+carry)/10;
            result.next = new ListNode(sum);
            if(l1!=null)
                l1 = l1.next;
            if(l2!=null)
                l2 = l2.next;
            result = result.next;
        }
        if(carry!=0) result.next = new ListNode(carry);
        return head.next;
    }
    public static void deleteNode(ListNode node) {
        while (node.next.next != null) {
            node.val = node.next.val;
            node = node.next;
        }
        node.val = node.next.val;
        node.next = null;
    }
    public static int getDecimalEquivqlent(ListNode head) {
        int size = 0;
        ListNode currentNode = head;
        while (currentNode != null) {
            size++;
            currentNode = currentNode.next;
        }
        size--;
        int result = 0;
        ListNode current = head;
        while (size >= 0 && current != null) {
            result += current.val * ((int) Math.pow(2, size));
            current = current.next;
            size--;
        }
        return result;
    }
    public static void printNextGreaterNodeValues(ListNode head){
        ListNode currNode =head, nextNode;
        int size=0,i=0;
        while (currNode != null) {
            size++;
            currNode = currNode.next;
        }
        currNode =head;
        int[] arr = new int[size];
            while(currNode != null && i<size){
                nextNode = currNode.next;
                int flag=0;
                while(nextNode!=null){
                if (nextNode.val > currNode.val)
                {
                    arr[i] = nextNode.val;
                    flag=1;
                    break;
                }
                nextNode = nextNode.next;

            }
            if(flag ==0)
                arr[i]=0;
            i++;
            currNode = currNode.next;
        }
        for(int x : arr)
        {
            System.out.print(x + " ");
        }
    }
    public static ListNode reverseIteratively(ListNode node)
    {
        ListNode prev = null;
        ListNode current = node;
        ListNode next = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }
    public static ListNode reverse(ListNode head)
    {
        if (head == null || head.next == null)
            return head;

        ListNode rest = reverse(head.next);
        head.next.next = head;

        head.next = null;
        return rest;
    }
    public static ListNode partitionList(ListNode head, int x){
        ListNode tail = head;
        ListNode curr = head;
        while (curr != null)
        {
            ListNode next = curr.next;
            if (curr.val < x)
            {
                curr.next = head;
                head = curr;
            }

            else
            {
                tail.next = curr;
                tail = curr;
            }
            curr = next;
        }
        tail.next = null;
        return head;
    }
    public static ListNode removeAllDuplicates(ListNode head){
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy;
        ListNode current = head;

        while (current != null)
        {
            while (current.next != null &&
                    prev.next.val == current.next.val)
                current = current.next;
            if (prev.next == current)
                prev = prev.next;
            else
                prev.next = current.next;

            current = current.next;
        }
        head = dummy.next;
        return head;
    }
    public static ListNode mergeSortedLists(ListNode l1, ListNode l2){
        if(l1 == null) return l2;
        if(l2 == null) return l1;

        if(l1.val < l2.val)
        {
            l1.next = mergeSortedLists(l1.next, l2);
            return l1;
        }
        else
        {
            l2.next = mergeSortedLists(l1, l2.next);
            return l2;
        }

    }
}


public class LinkedListOperations {
    public static void main(String[] args) {
        System.out.println("LinkedList Operations - Enter your choice");
        System.out.println("1. Addition of 2 numbers\n2. Delete a node\n3. Convert Binary to Integer\n4. Next Greater Nodes\n5. Partition List\n6. Reverse\n7.Remove All Duplicates \n8. Merge sorted lists");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        switch(choice)
        {
            case 1:
                System.out.println("Enter the first LinkedList");
                ListNode head1 = LinkedList.createList();
                LinkedList.printList(head1);
                System.out.println("Enter the second LinkedList");
                ListNode head2 = LinkedList.createList();
                LinkedList.printList(head2);
                ListNode resultHead = LinkedList.addTwoNumbers(head1,head2);
                System.out.println("The sum of LinkedLists");
                LinkedList.printList(resultHead);
                break;
            case 2:
                System.out.println("Enter the LinkedList");
                ListNode head = LinkedList.createList();
                System.out.println("Enter the node to be deleted");
                int nodeVal = sc.nextInt();
                ListNode curNode,delNode;
                curNode= head;
                while(curNode!=null){
                    if(curNode.val == nodeVal)
                    {   delNode = curNode;
                    break;}
                    curNode = curNode.next;
                }
                LinkedList.deleteNode(curNode);
                LinkedList.printList(head);
                break;
            case 3:
                System.out.println("Enter the Binary LinkedList");
                ListNode binaryHead = LinkedList.createList();
                LinkedList.printList(binaryHead);
                System.out.println("The decimal equivalent is : "+ LinkedList.getDecimalEquivqlent(binaryHead));
                break;
            case 4:
                System.out.println("Enter the LinkedList");
                ListNode hd = LinkedList.createList();
                LinkedList.printList(hd);
                System.out.println("The next greater node values are :");
                LinkedList.printNextGreaterNodeValues(hd);
                break;
            case 5:
                System.out.println("Enter the LinkedList");
                ListNode head3 = LinkedList.createList();
                LinkedList.printList(head3);
                System.out.print("Enter the partition value : ");
                int x= sc.nextInt();
                ListNode head4 =LinkedList.partitionList(head3, x);
                LinkedList.printList(head4);
                break;
            case 6:
                System.out.println("Enter the LinkedList");
                ListNode hd6 = LinkedList.createList();
                LinkedList.printList(hd6);
                System.out.println("After reversal(Iteratively) :");
                ListNode newHead =LinkedList.reverseIteratively(hd6);
                LinkedList.printList(newHead);
                System.out.println("After reversal(Recursively) :");
                ListNode newHead1 =LinkedList.reverse(newHead);
                LinkedList.printList(newHead1);
                break;
            case 7:
                System.out.println("Enter the LinkedList");
                ListNode newHd = LinkedList.createList();
                LinkedList.printList(newHd);
                System.out.println("After removing all duplicates :");
                LinkedList.printList(LinkedList.removeAllDuplicates(newHd));
            case 8:
                System.out.println("Enter first sorted LinkedList");
                ListNode l1 = LinkedList.createList();
                LinkedList.printList(l1);
                System.out.println("Enter second sorted LinkedList");
                ListNode l2 = LinkedList.createList();
                LinkedList.printList(l2);
                System.out.println("After merging the 2 sorted lists");
                LinkedList.printList(LinkedList.mergeSortedLists(l1,l2));
            default :
                break;
        }


    }
}
