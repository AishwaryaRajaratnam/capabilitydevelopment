import java.util.*;
public class StringReversal {
//String Reversal using Iteration or Recursion - Take sample String and print reverse of String characters.
    public static String reverseString(String str){
        if(str.isEmpty()){
            return str;
        }
        return reverseString(str.substring(1)) + str.charAt(0);
    }
    public static void main(String ...args){

        System.out.println("Enter a String");
        Scanner sc= new Scanner(System.in);
        String str = sc.nextLine();
        if(null == str || str.isEmpty()){
            System.out.println("Please enter a valid String");
        }
        else {
            System.out.println("You have entered :" + str);
            System.out.println("The reversed String is  :" +reverseString(str));
        }
    }
}
