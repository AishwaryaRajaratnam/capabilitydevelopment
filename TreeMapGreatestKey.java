import java.util.*;
public class TreeMapGreatestKey {
    public static void main(String args[]) {
        TreeMap<Integer, String> tm = new TreeMap<Integer, String>();
        tm.put(50, "White");
        tm.put(20, "Green");
        tm.put(30, "Black");
        tm.put(60, "Pink");
        tm.put(40, "Yellow");
        tm.put(10, "Red");

        System.out.println("The TreeMap is: " + tm);
        System.out.println("Greatest key less than or equal to the 5: " + tm.floorKey(5));
        System.out.println("Greatest key less than or equal to the 30: " + tm.floorKey(30));
        System.out.println("Greatest key less than or equal to the 100: " + tm.floorKey(100));
    }
}