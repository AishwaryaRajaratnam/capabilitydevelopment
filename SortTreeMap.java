import java.util.*;
class Employee{
    int empId;
    String empName;
    int salary;
    Employee(int empId, String empName, int salary){
        this.empId=empId;
        this.empName=empName;
        this.salary= salary;
    }
}
class EmpNameComp implements Comparator<Employee>{
    @Override
     public int compare(Employee e1, Employee e2) {
        return e1.empName.compareTo(e2.empName);
    }

}
public class SortTreeMap {
    public static void main(String ...args) {

        TreeMap<Employee,String> tree_map1 = new TreeMap<Employee,String>(new EmpNameComp());
        Employee e1 = new Employee(10,"Aishwarya", 50000);
        Employee e2 = new Employee(5,"Aathira", 150000);
        Employee e3 = new Employee(20,"Lina", 75000);
        tree_map1.put(e1,"SSE");
        tree_map1.put(e2,"Lead");
        tree_map1.put(e3,"Manager");
        System.out.println(tree_map1);
        Set<Employee> keys = tree_map1.keySet();
        for(Employee key:keys){
            System.out.println(key.empName+" ==> "+tree_map1.get(key));
        }
    }
}
