import java.util.*;
public class EmptyHashSet {

    public static void main(String ...args) {
        HashSet<String> hset = new HashSet<>();
        hset.add("India");
        hset.add("Japan");
        hset.add("Germany");
        hset.add("Singapore");
        hset.add("Japan");
        System.out.println("HashSet elements : " + hset);
        hset.clear();
        System.out.println("HashSet elements after clearing: " + hset);
    }
}
