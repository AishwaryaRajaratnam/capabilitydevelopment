import java.util.*;
import java.lang.*;
/*Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.*/
public class MergeAndSortArrays {

    public static void mergeAndSortIntArrays(int[] arr1, int[] arr2){
        ArrayList<Integer> arrList = new ArrayList<Integer>();

        for (int i = 0; i < arr1.length; i++)
            arrList.add(new Integer(arr1[i]));
        for (int i = 0; i < arr2.length; i++)
            arrList.add(new Integer(arr2[i]));
        Collections.sort(arrList);
        //Integer[] numArray = arrList.stream().toArray( n -> new Integer[n]);
        System.out.print(arrList);
    }

    public static void main(String ...args){

        Scanner sc= new Scanner(System.in);
        System.out.print("Enter the size of Array1: ");
        int size1 = sc.nextInt();
        int[] arr1 = new int[size1];
        System.out.print("Enter the first sorted array of integers ");
        for(int i=0;i<size1;i++){
            arr1[i] = sc.nextInt();
        }
        System.out.print("Enter the size of Array2: ");
        int size2 = sc.nextInt();
        int[] arr2 = new int[size2];
        System.out.print("Enter the second sorted array of integers ");
        for(int i=0;i<size2;i++){
            arr2[i] = sc.nextInt();
        }
        System.out.print("Array1 : ");
        for(int n : arr1){
            System.out.print(n + " ");
        }
        System.out.print("Array2 : ");
        for(int n : arr2){
            System.out.print(n + " ");
        }
        System.out.print("The merged sorted array :");
        mergeAndSortIntArrays(arr1,arr2);
    }
}
