import java.util.*;
public class HashSetRetainAll {
    public static void main(String ...args) {
        HashSet<String> hset = new HashSet<>();
        hset.add("India");
        hset.add("Japan");
        hset.add("Germany");
        hset.add("Singapore");
        System.out.println("First HashSet elements : " + hset);
        HashSet<String> hset1 = new HashSet<>();
        hset1.add("India");
        hset1.add("Italy");
        hset1.add("Germany");
        hset1.add("Russia");
        System.out.println("Second HashSet elements : " + hset1);
        hset.retainAll(hset1);
        System.out.println("HashSet elements : " + hset);

    }
}
