import java.util.*;
public class CheckSubstring {
	//Take two strings, check if one string is a substring of another.
    public static int isSubstring(String s1, String s2){
        int len1 = s1.length();
        int len2 = s2.length();
        for(int i=0; i<len1-len2 ; i++){
            for (int j=0;j<len2;j++){
                if(s1.charAt(i+j) != s2.charAt(j)){
                    break;
                }
                if(j == len2-1)
                    return i;
            }

        }
        return -1;
    }
    public static void main(String ...args){

        System.out.println("Enter String1");
        Scanner sc= new Scanner(System.in);
        String str1 = sc.nextLine();
        System.out.println("Enter String2");
        String str2 = sc.nextLine();
        if(null == str1 || str1.isEmpty() || null == str2 || str2.isEmpty() ){
            System.out.println("Please enter valid String");
        }
        else {
            if(isSubstring(str1,str2)>=0)
             System.out.println("String2  :" + str2 + " is a substring of String1 : " +str1);
            else
                System.out.println("String2  :" + str2 + " is not a substring of String1 : " +str1);
        }
    }
}
