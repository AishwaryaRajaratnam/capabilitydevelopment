import java.util.*;
public class BinarySearch {

    public static int doBinarySearch(int[] arr, int first , int last, int num){
    if(last>=first) {
        int mid = (first + last) / 2;
        if (num == arr[mid])
            return mid;
        if (num < arr[mid])
            return doBinarySearch(arr, first, mid - 1, num);
        return doBinarySearch(arr, mid + 1, last, num);
    }
        return -1;
    }
    public static void main(String ...args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size of Array: ");
        int size1 = sc.nextInt();
        int[] arr = new int[size1];
        System.out.print("Enter the sorted array of integers ");
        for (int i = 0; i < size1; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.print("Enter the number to be searched : ");
        int num = sc.nextInt();
        int result = doBinarySearch(arr, 0, arr.length -1 , num);
        if (result == -1)
            System.out.println("Element not present");
        else
            System.out.println("Element found at index " + result);
    }
}
