import java.util.*;
public class LongestPallindrome {
	/*For given String consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.*/
    public static int longestePallindromeLength(String str){
        int maxLen=1;
        for(int gap =str.length(); gap>=2 ;gap--){
            for(int i=0,j=i+gap; i<str.length() && j<=str.length() ; ++i, j=i+gap ){
                String subStr = str.substring(i,j);
                if(subStr.length()>=2){
                    StringBuilder sb = new StringBuilder(subStr);
                    String reversedSubStr = sb.reverse().toString();
                    if(subStr.equalsIgnoreCase(reversedSubStr))
                        maxLen =gap;
                }
            }
            if(maxLen == gap)
                return maxLen;
        }
        return maxLen;
    }
    public static void main(String ...args){

        System.out.println("Enter a String");
        Scanner sc= new Scanner(System.in);
        String str = sc.nextLine();
        if(null == str || str.isEmpty()){
            System.out.println("Please enter a valid String");
        }
        else {
            System.out.println("You have entered :" + str.toLowerCase());
            System.out.println(" Length of longest pallindrome in the String is : "+ longestePallindromeLength(str));
        }
    }
}
