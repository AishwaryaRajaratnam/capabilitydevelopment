import java.util.*;
public class CountPallindromicSubstring {
	//For given string, count how many palindromic substrings are in the string.
    public static int countPallindromicSunbstrings(String str){
        int count =0;
        for(int i=0; i< str.length()-1; i++){
            for (int j= i+1; j<= str.length() ; j++){
                String subStr = str.substring(i,j);
                if(subStr.length()>=2){
                    StringBuilder sb = new StringBuilder(subStr);
                    String reversedSubStr = sb.reverse().toString();
                    if(subStr.equalsIgnoreCase(reversedSubStr))
                        count++;
                }
            }
        }

        return count;
    }
    public static void main(String ...args){

        System.out.println("Enter a String");
        Scanner sc= new Scanner(System.in);
        String str = sc.nextLine();
        if(null == str || str.isEmpty()){
            System.out.println("Please enter a valid String");
        }
        else {
            System.out.println("You have entered :" + str);
            int count= countPallindromicSunbstrings(str);
            System.out.println(" Number of Pallindromic substrings : "+ count);
        }
    }
}
